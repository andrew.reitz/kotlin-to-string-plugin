package cash.andrew.tostring

import cash.andrew.kotlin.tostring.ToString

@ToString
class Test(val a: String, val b: String)
