package cash.andrew.tostring

import cash.andrew.kotlin.tostring.ToString

@ToString
class Stuff(val lol: String, val b: Int = 10)