package cash.andrew.kotlin.tostring

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import org.jetbrains.kotlin.cli.common.CLIConfigurationKeys
import org.jetbrains.kotlin.cli.common.messages.MessageRenderer
import org.jetbrains.kotlin.cli.common.messages.PrintingMessageCollector
import org.jetbrains.kotlin.cli.jvm.compiler.EnvironmentConfigFiles
import org.jetbrains.kotlin.cli.jvm.compiler.KotlinCoreEnvironment
import org.jetbrains.kotlin.com.intellij.openapi.util.Disposer
import org.jetbrains.kotlin.com.intellij.openapi.util.text.StringUtilRt
import org.jetbrains.kotlin.com.intellij.psi.PsiFileFactory
import org.jetbrains.kotlin.config.CompilerConfiguration
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.idea.KotlinLanguage
import org.jetbrains.kotlin.psi.KtFile
import java.io.File

open class ToStringTask : DefaultTask() {
    @get:Internal
    var kotlinCompile: KotlinCompile? = null

    @get:OutputDirectory
    var outputDir: File? = null

    @get:InputFiles
    var sources: Iterable<File>? = null

    @TaskAction
    fun addToString() {
        val newFiles = mutableListOf<File>()
        val oldFiles = mutableListOf<File>()

        sources?.forEach { file ->
            val ktFile = psiFileFactory.createFileFromText(
                    file.name,
                    KotlinLanguage.INSTANCE,
                    StringUtilRt.convertLineSeparators(file.readText())
            ) as? KtFile ?: throw IllegalStateException("kotlin file expected")

            val output = File(outputDir, file.name)

            newFiles.add(output)
            oldFiles.add(file)

            val newFileText = ToStringCreator(ktFile).newFileText ?: return@forEach

            output.createNewFile()
            output.writeText(newFileText)
        }

        kotlinCompile?.source(newFiles)
//        kotlinCompile?.exclude { it.file in oldFiles }
    }
}
