package cash.andrew.kotlin.tostring

import org.jetbrains.kotlin.psi.KtClass
import org.jetbrains.kotlin.psi.KtTreeVisitorVoid
import java.io.File

class ToStringFinder(file: File) : KtTreeVisitorVoid() {

    init {
        createKtFile(file).accept(this)
    }

    private var _hasToStringAnnotation = false

    val hasToStringAnnotation: Boolean get() = _hasToStringAnnotation

    override fun visitClass(klass: KtClass) {
        klass.annotationEntries.map { it.shortName }.firstOrNull { it?.identifier == "ToString" } ?: return
        _hasToStringAnnotation = true
    }
}