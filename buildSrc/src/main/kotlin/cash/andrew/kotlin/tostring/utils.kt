package cash.andrew.kotlin.tostring

import org.jetbrains.kotlin.cli.common.CLIConfigurationKeys
import org.jetbrains.kotlin.cli.common.messages.MessageRenderer
import org.jetbrains.kotlin.cli.common.messages.PrintingMessageCollector
import org.jetbrains.kotlin.cli.jvm.compiler.EnvironmentConfigFiles
import org.jetbrains.kotlin.cli.jvm.compiler.KotlinCoreEnvironment
import org.jetbrains.kotlin.com.intellij.openapi.util.Disposer
import org.jetbrains.kotlin.com.intellij.openapi.util.text.StringUtilRt
import org.jetbrains.kotlin.com.intellij.psi.PsiFileFactory
import org.jetbrains.kotlin.config.CompilerConfiguration
import org.jetbrains.kotlin.idea.KotlinLanguage
import org.jetbrains.kotlin.psi.KtFile
import java.io.File

// junk drawer

private val kotlinProject = KotlinCoreEnvironment.createForProduction(
        Disposer.newDisposable(),
        CompilerConfiguration().apply {
            put(
                    CLIConfigurationKeys.MESSAGE_COLLECTOR_KEY,
                    PrintingMessageCollector(System.err, MessageRenderer.PLAIN_FULL_PATHS, false)
            )
        },
        EnvironmentConfigFiles.JVM_CONFIG_FILES
).project

internal val psiFileFactory = PsiFileFactory.getInstance(kotlinProject)

internal fun createKtFile(file: File): KtFile = psiFileFactory.createFileFromText(
        file.name,
        KotlinLanguage.INSTANCE,
        StringUtilRt.convertLineSeparators(file.readText())
) as? KtFile ?: throw IllegalStateException("kotlin file expected")
