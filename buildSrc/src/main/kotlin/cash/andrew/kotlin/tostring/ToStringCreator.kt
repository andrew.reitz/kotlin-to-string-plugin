package cash.andrew.kotlin.tostring

import org.jetbrains.kotlin.psi.KtClass
import org.jetbrains.kotlin.psi.KtFile
import org.jetbrains.kotlin.psi.KtTreeVisitorVoid
import org.jetbrains.kotlin.psi.psiUtil.getValueParameters
import org.jetbrains.kotlin.psi.psiUtil.isPublic

class ToStringCreator(private val ktFile: KtFile) : KtTreeVisitorVoid() {

    private val newClassText: MutableList<String> = mutableListOf()
    private val original: MutableList<String> = mutableListOf()

    init {
        ktFile.accept(this)
    }

    val newFileText: String?
        get() {
            if (newClassText.isEmpty()) {return null }

            var output = ktFile.text
            newClassText.zip(original).forEach { (new, original) ->
                output = output.replace(original, new)
            }

            return output
        }

    override fun visitClass(klass: KtClass) {
        klass.annotationEntries.map { it.shortName }.firstOrNull { it?.identifier == "ToString" } ?: return

        val params = klass.getValueParameters()
                .filter { it.isPublic }
                .map { it.name }

        val properties = klass.getProperties()
                .filter { it.isPublic }
                .map { it.name }

        val toStringText = (params + properties).joinToString(separator = ", $", prefix = "$")

        original.add(klass.text)

        if (klass.body == null) {
            newClassText.add("""${klass.text} {
                |    // generated to string
                |    override fun toString() = "${klass.name}($toStringText)"
                |}
                """.trimMargin()
            )

            return
        }

        newClassText.add("""${klass.text.removeSuffix("}")}
        |    // generated to string
        |    override fun toString() = "Sup"
        |}
        """.trimMargin()
        )
    }
}
