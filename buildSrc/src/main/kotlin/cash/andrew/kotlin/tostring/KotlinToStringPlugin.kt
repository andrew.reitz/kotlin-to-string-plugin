package cash.andrew.kotlin.tostring

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.kotlin.dsl.register
import org.gradle.kotlin.dsl.the
import org.gradle.kotlin.dsl.withType
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Suppress("LocalVariableName")
class KotlinToStringPlugin : Plugin<Project> {
    override fun apply(target: Project): Unit = target.run {
        val namedSourceSet = target.the<SourceSetContainer>().map {
            it.getCompileTaskName("kotlin") to it.allSource
        }.toMap()

        tasks.withType<KotlinCompile> {
            logger.debug("creating task for {}", name)

            val _kotlinCompile = this
            val _outputDir = file("$buildDir/generated/toString").also { assert(it.mkdirs()) }

            val sourceSet = namedSourceSet[name]

            val filesWithToString = sourceSet?.asSequence()
                    ?.filter { ToStringFinder(it).hasToStringAnnotation }
                    ?.toList() ?: emptyList()

            exclude { it.file in filesWithToString }

            val toString = tasks.register<ToStringTask>("generateToString${name.capitalize()}") {
                kotlinCompile = _kotlinCompile
                sources = sourceSet?.asIterable()
                outputDir = _outputDir
            }

            dependsOn(toString)
        }
    }
}