plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("gradle-plugin", "1.3.30"))
    implementation(kotlin("compiler-embeddable"))
}

gradlePlugin {
    plugins {
        register("to-string-plugin") {
            id = "cash.andrew.kotlin.to-string"
            implementationClass = "cash.andrew.kotlin.tostring.KotlinToStringPlugin"
        }
    }
}
