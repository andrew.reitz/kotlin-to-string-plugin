# Kotlin-to-string-plugin

Code gen on kotlin. Automatically add toString implementations with just a single annotation. Similar
to how the `@ToString` annotation works in groovy.

- Caching works!

This is a work in progress.

Known issues:

- Code is very bad.
- no tests.
- probably slow on large projects.
- Can't use `@ToString` in the main file of an application project.
