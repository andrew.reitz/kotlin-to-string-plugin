plugins {
    application
    kotlin("jvm")
}

apply(plugin = "cash.andrew.kotlin.to-string")

allprojects {
    repositories {
        jcenter()
    }
}

dependencies {
    implementation(project(":annotations"))

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("compiler-embeddable"))
}

application {
    mainClassName = "cash.andrew.tostring.AppKt"
}
